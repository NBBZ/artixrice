#      ___                                       ___
#     /\  \         _____         _____         /\__\
#     \:\  \       /::\  \       /::\  \       /::|  |
#      \:\  \     /:/\:\  \     /:/\:\  \     /:/:|  |
#  _____\:\  \   /:/ /::\__\   /:/ /::\__\   /:/|:|  |__ 
# /::::::::\__\ /:/_/:/\:|__| /:/_/:/\:|__| /:/ |:| /\__\
# \:\~~\~~\/__/ \:\/:/ /:/  / \:\/:/ /:/  / \/__|:|/:/  / spectrwm config
#  \:\  \        \::/_/:/  /   \::/_/:/  /      |:/:/  /
#   \:\  \        \:\/:/  /     \:\/:/  /       |::/  /
#    \:\__\        \::/  /       \::/  /        |:/  /
#     \/__/         \/__/         \/__/         |/__/

# General settings {{{
workspace_limit=10
focus_mode=default
focus_close=previous
focus_close_wrap=1
focus_default=first
spawn_position=next
workspace_clamp=1
warp_focus=1
warp_pointer=1
modkey=Mod4
# }}}

# Window Decoration {{{
border_width=2
region_padding=1
tile_gap=4
color_focus=rgb:00/ff/00
color_focus_maximized=rgb:00/ff/00
color_unfocus=rgb:64/64/64
color_unfocus_maximized=rgb:64/64/64
# }}}

# Bar Settings {{{
bar_action=~/.config/spectrwm/baraction.sh
bar_enabled=1
bar_border_width=0
bar_color=rgb:00/00/00
bar_font_color=rgb:ff/ff/ff
bar_font=Mononoki Nerd Font:size=9:style=Bold
bar_justify=left
bar_format=[+L] +S [+W] +|R +A [%R] [%a %b %d]
workspace_indicator=listcurrent,listactive,markcurrent
bar_at_bottom=0
maximize_hide_bar=1
# }}}

# Workspaces {{{
name=ws[1]:1
name=ws[2]:2
name=ws[3]:3
name=ws[4]:4
name=ws[5]:5
name=ws[6]:6
name=ws[7]:7
name=ws[8]:8
name=ws[9]:9
name=ws[10]:10
# }}}

# Keybindings {{{
bind[focus_next]=MOD+j
bind[focus_prev]=MOD+k
bind[master_grow]=MOD+l
bind[master_shrink]=MOD+h
bind[master_add]=MOD+Shift+h
bind[master_del]=MOD+Shift+l

bind[swap_next]=MOD+Shift+j
bind[swap_prev]=MOD+Shift+k
bind[wind_del]=MOD+Shift+q
bind[wind_kill]=MOD+Shift+x

bind[maximize_toggle]=MOD+f
bind[cycle_layout]=MOD+r
bind[flip_layout]=MOD+Shift+f
bind[float_toggle]=MOD+space

bind[ws_1]=MOD+1
bind[ws_2]=MOD+2
bind[ws_3]=MOD+3
bind[ws_4]=MOD+4
bind[ws_5]=MOD+5
bind[ws_6]=MOD+6
bind[ws_7]=MOD+7
bind[ws_8]=MOD+8
bind[ws_9]=MOD+9
bind[ws_10]=MOD+0
bind[mvws_1]=MOD+Shift+1
bind[mvws_2]=MOD+Shift+2
bind[mvws_3]=MOD+Shift+3
bind[mvws_4]=MOD+Shift+4
bind[mvws_5]=MOD+Shift+5
bind[mvws_6]=MOD+Shift+6
bind[mvws_7]=MOD+Shift+7
bind[mvws_8]=MOD+Shift+8
bind[mvws_9]=MOD+Shift+9
bind[mvws_10]=MOD+Shift+0
bind[quit]=MOD+Control+q
bind[restart]=MOD+Shift+r

program[osu!mode]=xrandr --output HDMI-0 --off
bind[osu!mode]=MOD+y

program[resetmonitors]=xrandr --output HDMI-0 --auto --output DP-0 --auto --primary --left-of HDMI-0 --output DP-0 --mode 1920x1080 --rate 144.00
bind[resetmonitors]=MOD+Shift+y

program[playpause]=playerctl play-pause
bind[playpause]=XF86AudioPlay
program[nexttrack]=playerctl next
bind[nexttrack]=XF86AudioNext
program[prevtrack]=playerctl previous
bind[prevtrack]=XF86AudioPrev

program[volmute]=pulsemixer --toggle-mute
bind[volmute]=XF86AudioMute
bind[volmute]=Shift+XF86AudioPlay
program[volup]=pulsemixer --change-volume +10
bind[volup]=Shift+XF86AudioNext
bind[volup]=XF86AudioRaiseVolume
program[voldown]=pulsemixer --change-volume -10
bind[voldown]=Shift+XF86AudioPrev
bind[voldown]=XF86AudioLowerVolume

program[alacritty]=alacritty
bind[alacritty]=MOD+e

program[rofi_drun]=rofi -show drun
bind[rofi_drun]=MOD+d

program[rofi_run]=rofi -show run
bind[rofi_run]=MOD+c

program[pulsemixer]=alacritty -e pulsemixer
bind[pulsemixer]=MOD+v

program[scrot]=scrot -q 100 '%c.png'
bind[scrot]=MOD+Print

program[pcmanfm]=pcmanfm
bind[pcmanfm]=MOD+g

program[browser]=qutebrowser
bind[browser]=MOD+t

program[calculator]=gnome-calculator
bind[calculator]=MOD+Shift+c

program[spotify]=spotify
bind[spotify]=MOD+Shift+s

program[moc]=alacritty -e mocp --moc-dir ~/.config/moc
bind[moc]=MOD+s

program[discord]=discord
bind[discord]=MOD+Shift+d
# }}}
