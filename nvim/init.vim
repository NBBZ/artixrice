"      ___                                       ___     
"     /\  \         _____         _____         /\__\    
"     \:\  \       /::\  \       /::\  \       /::|  |   
"      \:\  \     /:/\:\  \     /:/\:\  \     /:/:|  |   
"  _____\:\  \   /:/ /::\__\   /:/ /::\__\   /:/|:|  |__ 
" /::::::::\__\ /:/_/:/\:|__| /:/_/:/\:|__| /:/ |:| /\__\
" \:\~~\~~\/__/ \:\/:/ /:/  / \:\/:/ /:/  / \/__|:|/:/  / nvim config
"  \:\  \        \::/_/:/  /   \::/_/:/  /      |:/:/  / 
"   \:\  \        \:\/:/  /     \:\/:/  /       |::/  /  
"    \:\__\        \::/  /       \::/  /        |:/  /   
"     \/__/         \/__/         \/__/         |/__/    

" enable syntax highlighting
syntax enable

" true color
set termguicolors

" colorscheme
let g:blackspace_italics=1
colorscheme blackspace

" line numbers
set nu

" add mouse funcionality
set mouse=a

" highlight current row and column
set cursorline
set cursorcolumn

" new line without insert mode with enter
nmap <S-Enter> 0<Esc>
nmap <CR> o<Esc>

" clear highlighting for search with F3
nmap <F3> :noh<CR>

" fold method
set foldmethod=marker

" add window title
set title

" Syntax highlighting for different files
autocmd BufNewFile,BufRead *.rasi set ft=css
autocmd BufNewFile,BufRead *aliases set ft=zsh
autocmd BufNewFile,BufRead *.conf set ft=sh
