#!/usr/bin/env bash
DEST=$HOME/artixrice

mkdir -p $HOME/.config 2>&1 > /dev/null

link(){
	ln -s $DEST/$1 $HOME/.config/ 
}

config_files=("alacritty" "rofi" "picom" "spectrwm" "mpv" "moc" "lf" "lsd" "zathura" "cava" "dunst" "qutebrowser" "x11" "zsh" "spicetify" "nvim" "BetterDiscord")

for file in ${config_files[@]}
do
	link $file
done

ln -s $DEST/.zprofile $HOME/
